package com.bmxstore.grind_store.db.entity.order;

public enum OrderStatus {
    NEW, PAID, PAYMENT_FAILED, CANCELLED, SHIPPED, DELIVERED, CLOSED
}
