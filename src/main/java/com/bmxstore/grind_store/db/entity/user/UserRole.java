package com.bmxstore.grind_store.db.entity.user;

public enum UserRole {
    USER,
    ADMIN
}
