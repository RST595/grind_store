package com.bmxstore.grind_store.db.entity.product;

public enum ProductColor {
    BLACK, WHITE, YELLOW, GREEN, RED, BLUE, CYAN, VIOLET,
    PINK, PURPLE, BROWN, MIX, GREY, ORANGE, CHROME, TERRACOTTA
}
